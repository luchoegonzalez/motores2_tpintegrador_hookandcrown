# Motores2_TPIntegrador_HookAndCrown

![banner](https://img.itch.zone/aW1nLzE3NzQxMjE5LnBuZw==/original/OP7RGK.png)

:es: Videojuego de plataformas en 2 dimensiones para un jugador en el que podremos controlar a dos personajes, Rammel y Aelarion, a través de diversos niveles, teniendo la posibilidad de aprovechar las habilidades únicas de cada uno para resolver puzzles y enfrentar desafíos.
Hecho en Unity para la materia "Motores Gráficos II" de la Licenciatura en Producción de Simuladores y Videojuegos.

:us: A 2D platform game for a single player where you can control two characters, Rammel and Aelarion, through various levels, using their unique abilities to solve puzzles and face challenges.
Made in Unity for the "Game Engines II" course in the Bachelor's in Simulation and Video Game Production.

[Take a look at the creative process](https://luchoegonzalez.github.io/portfolio/games/hookandcrown.html)

[Play on itch.io](https://lucirro.itch.io/hookandcrown)