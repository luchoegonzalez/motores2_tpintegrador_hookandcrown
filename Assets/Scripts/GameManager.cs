using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Audio;

public class GameManager : MonoBehaviour
{
    public Personaje Aelarion;
    public Personaje Rammel;
    public GameObject indicadorVidasRammel;
    public GameObject indicadorVidasAelarion;
    public GameObject panelGameOver;
    public GameObject panelPerdiste;
    public GameObject panelPausa;
    public GameObject panelOpciones;

    public Slider sliderMusica;
    public Slider sliderSonidos;
    public AudioMixer mixer;

    public GameObject[] cajas;

    public Vector3 ultimoCheckpoint;

    public int contadorLombrices;
    public GameObject[] lombrices;

    public string siguienteNivel;

    bool muertoFinalizado;

    public int nivelActual;
    public bool nivelCompletado;

    private void Start()
    {
        ultimoCheckpoint = Rammel.transform.position;

        sliderMusica.value = PlayerPrefs.GetFloat("MusicVol");
        sliderSonidos.value = PlayerPrefs.GetFloat("SoundsVol");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftAlt) || Input.GetKeyDown(KeyCode.RightAlt))
        {
            Aelarion.controlandoPersonaje = !Aelarion.controlandoPersonaje;
            Rammel.controlandoPersonaje = !Rammel.controlandoPersonaje;
            Aelarion.GetComponent<AelarionControl>().followerAelarion.GetComponent<AudioSource>().Play();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            Reiniciar();
        }

        if (Input.GetKeyDown(KeyCode.Escape) && !muertoFinalizado)
        {
            Pausar();
        }
    }

    public void Reiniciar()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        if (Rammel.transform.parent != null)
        {
            Rammel.transform.parent = null;
        }
        foreach (GameObject caja in cajas)
        {
            if (caja.transform.parent != null)
            {
                caja.transform.parent = null;
            }
        }
    }

    private void OnGUI()
    {
        indicadorVidasAelarion.SetActive(Aelarion.controlandoPersonaje);
        indicadorVidasRammel.SetActive(Rammel.controlandoPersonaje);
    }

    public void GameOver(string texto)
    {
        Time.timeScale = 0;
        panelGameOver.SetActive(true);
        panelGameOver.transform.GetChild(0).GetComponent<TMP_Text>().text = texto;

        if (contadorLombrices > 0)
        {
            for (int i = 1; i <= contadorLombrices; i++)
            {
                lombrices[i-1].SetActive(true);
            }
        }

        muertoFinalizado = true;
    }

    public void Perdiste()
    {
        Time.timeScale = 0;
        panelPerdiste.SetActive(true);
        muertoFinalizado = true;
    }

    public void VolverAlMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MenuPrincipal");
        if (Rammel.transform.parent != null)
        {
            Rammel.transform.parent = null;
        }
        foreach (GameObject caja in cajas)
        {
            if (caja.transform.parent != null)
            {
                caja.transform.parent = null;
            }
        }
    }

    public void JugarSiguienteNivel()
    {
        SceneManager.LoadScene(siguienteNivel); Time.timeScale = 1;
        Time.timeScale = 1;
        if (Rammel.transform.parent != null)
        {
            Rammel.transform.parent = null;
        }
        foreach (GameObject caja in cajas)
        {
            if (caja.transform.parent != null)
            {
                caja.transform.parent = null;
            }
        }
    }

    public void Pausar()
    {
        Time.timeScale = 0;
        panelPausa.SetActive(true);
        panelOpciones.SetActive(false);
    }

    public void Reanudar()
    {
        Time.timeScale = 1;
        panelPausa.SetActive(false);
        panelOpciones.SetActive(false);
    }

    public void AbrirOpciones()
    {
        Time.timeScale = 0;
        panelPausa.SetActive(false);
        panelOpciones.SetActive(true);
    }

    public void AplicarVolumenMusica()
    {
        float volumen = sliderMusica.value;
        mixer.SetFloat("Music", Mathf.Log10(volumen) * 20);
        PlayerPrefs.SetFloat("MusicVol", volumen);
    }

    public void AplicarVolumenSonido()
    {
        float volumen = sliderSonidos.value;
        mixer.SetFloat("Sounds", Mathf.Log10(volumen) * 20);
        PlayerPrefs.SetFloat("SoundsVol", volumen);
    }
}
