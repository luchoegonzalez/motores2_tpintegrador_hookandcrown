using UnityEngine;

public class ProyectilControl : MonoBehaviour
{
    public float rapidez = 10f;
    BoxCollider2D col;

    private void Start()
    {
        col = GetComponent<BoxCollider2D>();
        Destroy(gameObject, 3.5f);
    }

    private void Update()
    {
        transform.Translate(Vector3.right * rapidez * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D hit)
    {
        Destroy(gameObject);
        if (hit.gameObject.CompareTag("Rammel") || hit.gameObject.CompareTag("Aelarion"))
        {
            hit.gameObject.GetComponent<Personaje>().RecibirDano(1, hit.transform.position);
        }
    }

    public void setearTarget(GameObject target)
    {
        if (target.CompareTag("Aelarion"))
        {
            gameObject.layer = LayerMask.NameToLayer("Aelarion");
        } else
        {
            gameObject.layer = LayerMask.NameToLayer("Default");
        }
    }
}