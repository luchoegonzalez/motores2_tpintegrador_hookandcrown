using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoVoladorControl : Enemigo
{
    [SerializeField] float rapidez = 2;
    public Vector3[] posiciones;
    private int posicionActual = 0;
    GameManager gameManager;
    Personaje rammel, aelarion;
    Transform target;

    SpriteRenderer render;
    public Sprite ataque;
    public Sprite iddle;

    // Start is called before the first frame update
    internal override void Start()
    {
        base.Start();
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        transform.position = posiciones[posicionActual];
        rammel = gameManager.Rammel.GetComponent<Personaje>();
        aelarion = gameManager.Aelarion.GetComponent<Personaje>();
        target = rammel.transform;
        render = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        Moverse();

        if (aelarion.controlandoPersonaje)
        {
            target = aelarion.transform;
        }
        else if (rammel.controlandoPersonaje)
        {
            target = rammel.transform;
        }

        if (target.position.x <= transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);

        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }

    void Moverse()
    {
        if (!atacando && !muerto)
        {
            transform.position = Vector3.MoveTowards(transform.position, posiciones[posicionActual], rapidez * Time.deltaTime);

            if (Vector3.Distance(transform.position, posiciones[posicionActual]) < 0.05f)
            {
                posicionActual = (posicionActual + 1) % posiciones.Length;
            }
        }
    }

    internal override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        render.sprite = ataque;
    }

    internal override void FinalizarAtaque()
    {
        base.FinalizarAtaque();
        render.sprite = iddle;
    }
}
