using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlataformaControl : MonoBehaviour
{
    public BotonControl boton;
    [SerializeField] float rapidez = 2;
    public Vector3[] posiciones;
    private int posicionActual = 0;
    Rigidbody2D jugador;
    Vector3 deltaMovimiento;
    Vector3 targetPosition;

    private void Start()
    {
        transform.position = posiciones[posicionActual];
    }

    private void Update()
    {

        //if (boton.botonActivo)
        //{
        //    posicionActual = 1;
        //} else
        //{
        //    posicionActual = 0;
        //}

        if (boton.botonActivo)
        {
            // Mover la plataforma hacia la posición actual
            targetPosition = posiciones[posicionActual];

            // Comprobar si la plataforma ha alcanzado la posición objetivo
            if (Vector3.Distance(transform.position, targetPosition) < 0.01f)
            {
                // Cambiar a la siguiente posición
                posicionActual = (posicionActual + 1) % posiciones.Length;
            }
        } else
        {
            posicionActual = 0;
        }


        //Calculate desired position
        Vector3 desiredPosition = Vector3.MoveTowards(transform.position, posiciones[posicionActual], rapidez * Time.deltaTime);

        //Use that position to figure out the change in position of the platform
        deltaMovimiento = new Vector3(desiredPosition.x, desiredPosition.y, 0f) - transform.position;

        //Apply the new position
        transform.position = desiredPosition;
    }

    private void LateUpdate()
    {
        if (jugador && Vector3.Distance(transform.position, posiciones[0]) > 0.01f)
        {
            Vector3 playerBody = jugador.position;
            jugador.transform.position = new Vector3(playerBody.x, playerBody.y, playerBody.z) + deltaMovimiento;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Caja"))
        {
            collision.gameObject.transform.parent = transform;
        }
        if (collision.gameObject.CompareTag("Rammel"))
            jugador = collision.gameObject.GetComponent<Rigidbody2D>();
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Caja"))
        {
            collision.gameObject.transform.parent = null;
        }
        if (collision.gameObject.CompareTag("Rammel"))
        {
            //fix eje z
            jugador.transform.position = new Vector3(jugador.transform.position.x, jugador.transform.position.y, 0);
            jugador = null;
        }
    }
}
