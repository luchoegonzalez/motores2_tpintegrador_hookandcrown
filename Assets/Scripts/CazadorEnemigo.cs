using UnityEngine;

public class CazadorEnemigo : Enemigo
{
    Transform puntoDeDisparo;
    public GameObject balaPrefabAelarion, balaPrefabRammel;
    GameObject balaPrefab;
    GameObject parteSuperior;
    GameManager gameManager;
    Personaje rammel, aelarion;
    Transform target;
    [SerializeField] float tasaDeDisparo;
    [SerializeField] float distanciaDeDisparo;
    float timerDisparo;

    Vector3 direccionDeDisparo;
    float anguloDeDisparo;

    [SerializeField] private SpriteRenderer ojo1, ojo2;
    public Sprite objetivoAelarion, objetivoRammel;

    internal override void Start()
    {
        parteSuperior = transform.GetChild(0).gameObject;
        puntoDeDisparo = parteSuperior.transform.GetChild(0).transform;
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        rammel = gameManager.Rammel.GetComponent<Personaje>();
        aelarion = gameManager.Aelarion.GetComponent<Personaje>();
    }

    void Update()
    {
        if (!atacando)
        {
            //if (rammel.controlandoPersonaje)
            //{
            //    target = rammel.transform;
            //} else if (aelarion.controlandoPersonaje)
            //{
            //    target = aelarion.transform;
            //}

            if(JugadorCerca())
            {
                direccionDeDisparo = (target.position - parteSuperior.transform.position).normalized;
                anguloDeDisparo = Mathf.Atan2(direccionDeDisparo.y, direccionDeDisparo.x) * Mathf.Rad2Deg;
                

                if (target.position.x <= transform.position.x)
                {
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                    parteSuperior.transform.localScale = new Vector3(-0.99f,0.99f,1);
                    parteSuperior.transform.rotation = Quaternion.Euler(0, 0, anguloDeDisparo - 180);

                }
                else
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    parteSuperior.transform.localScale = new Vector3(0.99f, 0.99f, 1);
                    parteSuperior.transform.rotation = Quaternion.Euler(0, 0, anguloDeDisparo);
                }
            }

            timerDisparo += Time.deltaTime;
            if (timerDisparo >= tasaDeDisparo && JugadorCerca())
            {
                Disparar(target);
                timerDisparo = 0;
            }
        }
    }

    bool JugadorCerca()
    {
        float distanciaAelarion = Vector3.Distance(transform.position, aelarion.transform.position);
        float distanciaRammel = Vector3.Distance(transform.position, rammel.transform.position);
        if (aelarion.controlandoPersonaje && (distanciaAelarion <= distanciaDeDisparo || distanciaRammel <= distanciaDeDisparo))
        {
            target = aelarion.transform;

            ojo1.enabled = true;
            ojo2.enabled = true;
            ojo1.sprite = objetivoAelarion;
            ojo2.sprite = objetivoAelarion;
            balaPrefab = balaPrefabAelarion;
            return true;
        } else if (rammel.controlandoPersonaje && distanciaRammel <= distanciaDeDisparo)
        {
            target = rammel.transform;

            ojo1.enabled = true;
            ojo2.enabled = true;
            ojo1.sprite = objetivoRammel;
            ojo2.sprite = objetivoRammel;
            balaPrefab = balaPrefabRammel;
            return true;
        } else
        {
            ojo1.enabled = false;
            ojo2.enabled = false;
            return false;
        }
    }

    //bool JugadorEstaCerca(Transform _target)
    //{
    //    float distancia = Vector3.Distance(transform.position, _target.position);
    //    if (_target.gameObject.CompareTag("Aelarion") && distancia >= distanciaDeDisparo)
    //    {
    //        return true;
    //    } else
    //    {
    //        return false;
    //    }
    //}

    void Disparar(Transform _target)
    {
        GameObject proyectil = Instantiate(balaPrefab, puntoDeDisparo.transform.position, Quaternion.Euler(0, 0, anguloDeDisparo));
        proyectil.GetComponent<ProyectilControl>().setearTarget(_target.gameObject);
        GetComponent<AudioSource>().Play();
    }
}
