using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BordesDeCamara : MonoBehaviour
{
    Camera camara;
    [SerializeField] LayerMask capasExcluidas; 
    void Start()
    {
        camara = GetComponent<Camera>();
        PosicionarColliders();
    }

    private void PosicionarColliders()
    {
        GameObject left = new GameObject("LeftCollider", typeof(BoxCollider2D));
        GameObject right = new GameObject("RightCollider", typeof(BoxCollider2D));
        GameObject top = new GameObject("TopCollider", typeof(BoxCollider2D));
        GameObject bottom = new GameObject("BottomCollider", typeof(BoxCollider2D));

        left.transform.SetParent(transform);
        right.transform.SetParent(transform);
        top.transform.SetParent(transform);
        bottom.transform.SetParent(transform);

        Vector2 leftBottomCorner = camara.ViewportToWorldPoint(Vector3.zero);
        Vector2 rightTopCorner = camara.ViewportToWorldPoint(Vector3.one);

        left.transform.position = new Vector2(
            leftBottomCorner.x - 0.5f,
            camara.transform.position.y
            );
        right.transform.position = new Vector2(
            rightTopCorner.x + 0.5f,
            camara.transform.position.y
            );
        top.transform.position = new Vector2(
            camara.transform.position.x,
            rightTopCorner.y + 0.5f
            );
        bottom.transform.position = new Vector2(
            camara.transform.position.x,
            leftBottomCorner.y - 0.5f
            );

        left.transform.localScale = new Vector3(1, Mathf.Abs(rightTopCorner.y - leftBottomCorner.y));
        right.transform.localScale = new Vector3(1, Mathf.Abs(rightTopCorner.y - leftBottomCorner.y));
        top.transform.localScale = new Vector3(Mathf.Abs(rightTopCorner.x - leftBottomCorner.x), 1);
        bottom.transform.localScale = new Vector3(Mathf.Abs(rightTopCorner.x - leftBottomCorner.x), 1);

        left.GetComponent<BoxCollider2D>().excludeLayers = capasExcluidas;
        right.GetComponent<BoxCollider2D>().excludeLayers = capasExcluidas;
        top.GetComponent<BoxCollider2D>().excludeLayers = capasExcluidas;
        bottom.GetComponent<BoxCollider2D>().excludeLayers = capasExcluidas;
    }
}
