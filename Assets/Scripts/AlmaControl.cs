using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlmaControl : MonoBehaviour
{
    public float rapidez;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 5f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y + 5, transform.position.z), rapidez * Time.deltaTime);
    }
}
