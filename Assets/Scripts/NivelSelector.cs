using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NivelSelector : MonoBehaviour
{
    public int nivel;
    public int cantidadLombrices;
    public bool completado;

    public GameObject[] lombricesUI;
    public GameObject coronaUI;

    private void Start()
    {
        for(int i = 0; i < cantidadLombrices; i++)
        {
            lombricesUI[i].SetActive(true);
        }

        if (completado)
        {
            coronaUI.SetActive(true);
        }
    }
}
