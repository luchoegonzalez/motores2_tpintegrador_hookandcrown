using UnityEngine;

public class AelarionControl : Personaje
{
    GameObject Rammel;
    Vector3 velocidadSeguimiento = Vector3.zero;
    [SerializeField] private float suavidadSeguimiento = 2f;
    public bool agarrandoCaja = false;
    CircleCollider2D col;

    //cajas
    public float radioDeDeteccion = 0.5f;
    public LayerMask capaCajas;
    private GameObject cajaAgarrada;
    private Rigidbody2D cajaRb;
    public Transform followerAelarion;

    //rotacion en seguimiento a Rammel
    float epsilon = 0.1f;

    Animator animator;
    Animator animatorAla1, animatorAla2;

    //sonidos
    float temporizador = 0f;
    public float tiempoEntreAleteo = 0.5f;
    AudioSource audioSource;
    public AudioClip[] clipsAleteo;
    public AudioClip clipDamage;

    private void Start()
    {
        Rammel = GameObject.Find("Rammel");
        animator = GetComponent<Animator>();
        animatorAla1 = transform.GetChild(0).GetComponent<Animator>();
        animatorAla2 = transform.GetChild(1).GetComponent<Animator>();
        col = gameObject.GetComponent<CircleCollider2D>();
        col.enabled = false;

        audioSource = GetComponent<AudioSource>();
    }

    internal override void Update()
    {
        temporizador -= Time.deltaTime;
        if (temporizador <= 0)
        {
            audioSource.pitch = Random.Range(1.1f, 1.25f);
            audioSource.PlayOneShot(clipsAleteo[Random.Range(0, clipsAleteo.Length)]);
            temporizador = tiempoEntreAleteo;
        }

        if (controlandoPersonaje)
        {
            if (recibiendoDano)
            {
                SoltarCaja();
            } else
            {
                movimientoVertical = Input.GetAxis("Vertical") * rapidezMovimiento;
                ControlarCajas();
            }

            CambiarDeCapa("Aelarion");
            animatorAla1.speed = 1;
            animatorAla2.speed = 1;
            tiempoEntreAleteo = 0.5f;
            col.enabled = true;
            base.Update();
        }
        else
        {
            SoltarCaja();
            col.enabled = false;

            transform.rotation = Rammel.transform.rotation;
            if (Mathf.Abs(Mathf.Abs(transform.rotation.eulerAngles.y) - 180) < epsilon)
            {
                mirandoDerecha = false;
            }
            else if (Mathf.Abs(Mathf.Abs(transform.rotation.eulerAngles.y) - 0) < epsilon)
            {
                mirandoDerecha = true;
            }

            if (Rammel.GetComponent<RammelControl>().planeando)
            {
                CambiarDeCapa("Aelarion");
                animatorAla1.speed = 2;
                animatorAla2.speed = 2;
                tiempoEntreAleteo = 0.25f;
                animator.SetBool("Agarrando", true);
                Vector3 destino = Rammel.transform.position + new Vector3(0, 1.1f, 0);
                transform.position = Vector3.SmoothDamp(transform.position, destino, ref velocidadSeguimiento, 0.03f);
            }
            else
            {
                CambiarDeCapa("Default");
                animatorAla1.speed = 1;
                animatorAla2.speed = 1;
                tiempoEntreAleteo = 0.5f;
                animator.SetBool("Agarrando", false);
                if (Rammel.GetComponent<RammelControl>().mirandoDerecha)
                {
                    Vector3 destino = Rammel.transform.position + new Vector3(-1.2f, 1.1f, 0);
                    transform.position = Vector3.SmoothDamp(transform.position, destino, ref velocidadSeguimiento, suavidadSeguimiento);
                }
                else
                {
                    Vector3 destino = Rammel.transform.position + new Vector3(1.2f, 1.1f, 0);
                    transform.position = Vector3.SmoothDamp(transform.position, destino, ref velocidadSeguimiento, suavidadSeguimiento);
                }
            }
        }
    }

    void ControlarCajas()
    {
        Collider2D[] cajas = Physics2D.OverlapCircleAll(transform.position - new Vector3(0, 0.2f, 0), radioDeDeteccion, capaCajas);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (cajas.Length > 0 && cajaAgarrada == null)
            {
                cajaAgarrada = cajas[0].gameObject;
                cajaRb = cajaAgarrada.GetComponent<Rigidbody2D>();
                cajaAgarrada.GetComponent<SpriteRenderer>().sortingLayerName = "Aelarion";
                cajaAgarrada.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Aelarion";
                cajaRb.isKinematic = true;
                cajaAgarrada.GetComponent<BoxCollider2D>().isTrigger = true;
                cajaAgarrada.transform.SetParent(followerAelarion);
                animator.SetBool("Agarrando", true);
            }
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            SoltarCaja();
        }
    }

    void SoltarCaja()
    {
        if (cajaAgarrada != null)
        {
            cajaAgarrada.transform.SetParent(null);
            cajaAgarrada.GetComponent<SpriteRenderer>().sortingLayerName = "Default";
            cajaAgarrada.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Default";
            cajaRb.isKinematic = false;
            cajaAgarrada.transform.position = new Vector3(cajaAgarrada.transform.position.x, cajaAgarrada.transform.position.y, 0);
            cajaAgarrada.GetComponent<BoxCollider2D>().isTrigger = false;
            cajaAgarrada = null;
            cajaRb = null;
            animator.SetBool("Agarrando", false);
        }
    }

    void CambiarDeCapa(string nombreDeCapa)
    {
        GetComponent<SpriteRenderer>().sortingLayerName = nombreDeCapa;
        transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = nombreDeCapa;
        transform.GetChild(1).GetComponent<SpriteRenderer>().sortingLayerName = nombreDeCapa;
    }

    internal override void Moverse()
    {
        if (controlandoPersonaje)
        {
            if (!recibiendoDano)
            {
                movimientoVertical *= 10f;
                movimientoVertical *= Time.fixedDeltaTime;
                base.Moverse();
            }
        }
        else
        {
            rb.velocity = Vector2.zero;
        }

    }

    internal override void RecibirDano(int dano, Vector3 posicionEnemigo)
    {
        Invoke("PermitirMovimiento", 0.3f);
        audioSource.PlayOneShot(clipDamage);
        base.RecibirDano(dano, posicionEnemigo);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position - new Vector3(0, 0.2f, 0), radioDeDeteccion);
    }
}