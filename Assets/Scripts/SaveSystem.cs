using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void GuardarNivel(GameManager gameManager, GameData previousData = null)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/progreso.hc";
        FileStream stream = new FileStream(path, FileMode.Create);

        GameData data = new GameData(gameManager, previousData);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static GameData CargarNiveles()
    {
        string path = Application.persistentDataPath + "/progreso.hc";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            GameData data = formatter.Deserialize(stream) as GameData;
            stream.Close();

            Debug.Log("Niveles Cargados: " + data);
            return data;
        }
        else
        {
            Debug.Log("No se encontraron archivos guardados");
            return null;
        }
    }
}
