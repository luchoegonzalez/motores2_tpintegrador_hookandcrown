using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using UnityEngine;

public class BotonControl : MonoBehaviour
{
    public float rangoDeteccion = 0.19f;
    public LayerMask capaRammelCajas;
    public bool botonActivo = false;
    SpriteRenderer render;
    public Sprite botonPresionado;
    Sprite botonDesactivado;

    AudioSource audioS;
    public AudioClip sonido1, sonido2;

    void Start()
    {
        render = GetComponent<SpriteRenderer>();
        botonDesactivado = render.sprite;
        audioS = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.CompareTag("Rammel") || hit.CompareTag("Caja"))
        {
            botonActivo = true;
            render.sprite = botonPresionado;
            audioS.PlayOneShot(sonido1);
            //col.offset = new Vector2 (col.offset.x, 0.98f);
        }
    }

    private void OnTriggerExit2D(Collider2D hit)
    {
        if (hit.CompareTag("Rammel") || hit.CompareTag("Caja"))
        {
            botonActivo = false;
            render.sprite = botonDesactivado;
            audioS.PlayOneShot(sonido2);
            //col.offset = new Vector2(col.offset.x, 1.680498f);
        }
    }

    private void OnTriggerStay2D(Collider2D hit)
    {
        if (hit.CompareTag("Rammel") || hit.CompareTag("Caja"))
        {
            botonActivo = true;
            render.sprite = botonPresionado;
            //col.offset = new Vector2 (col.offset.x, 0.98f);
        }
    }
}
