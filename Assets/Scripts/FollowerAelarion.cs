using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowerAelarion : MonoBehaviour
{
    GameObject aelarion;

    private void Awake()
    {
        aelarion = GameObject.Find("Aelarion");
    }

    void Update()
    {
        if (aelarion.activeInHierarchy)
        {
            transform.position = GameObject.Find("Aelarion").transform.position;
        }
    }
}
