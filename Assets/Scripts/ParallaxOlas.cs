using System;
using UnityEngine;


public class ParallaxOlas : MonoBehaviour
{
    internal float posicionInicialX, posicionInicialY, longitudSpriteX;
    public float valorParallaxX, valorParallaxY;
    public Camera MainCamera;

    internal float posicionTemporalX;

    internal virtual void Start()
    {
        posicionInicialX = transform.position.x;
        posicionInicialY = transform.position.y;
    }

    internal virtual void LateUpdate()
    {
        Vector3 posicionCamara = MainCamera.transform.position;
        posicionTemporalX = posicionCamara.x * (1 - valorParallaxX);
        float distanciaX = posicionCamara.x * valorParallaxX;
        float distanciaY = posicionCamara.y * valorParallaxY;

        Vector3 nuevaPosicion = new Vector3(posicionInicialX + distanciaX, posicionInicialY + distanciaY, transform.position.z);

        transform.position = nuevaPosicion;
    }
}
