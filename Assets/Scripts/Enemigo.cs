using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    [SerializeField] internal int vida = 1;
    [SerializeField] internal int danoDeAtaque = 1;
    internal bool atacando = false;
    internal bool muerto = false;
    [SerializeField] internal float cooldownAtaque = 0.5f;
    internal Animator animatorEnemigo;

    public GameObject alma;
    public Color colorAlma;

    public AudioClip[] clipsMuerte;

    internal virtual void Start()
    {
        animatorEnemigo = GetComponent<Animator>();
    }

    public void RecibirDano(int dano)
    {
        vida -= dano;

        if (vida <= 0)
        {
            muerto = true;
            GameObject almaInstancia = Instantiate(alma, transform.position, Quaternion.identity);
            almaInstancia.GetComponent<SpriteRenderer>().color = colorAlma;

            if (clipsMuerte.Length > 0)
            {
                almaInstancia.GetComponent<AudioSource>().PlayOneShot(clipsMuerte[Random.Range(0,clipsMuerte.Length)]);
            }

            if(animatorEnemigo != null)
            {
                animatorEnemigo.SetBool("Muerto", true);
                GetComponent<CapsuleCollider2D>().enabled = false;
                Destroy(gameObject, 5f);
            } else
            {
                Destroy(gameObject);
            }
        }
    }

    internal virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Rammel") || collision.gameObject.CompareTag("Aelarion"))
        {
            if (animatorEnemigo != null)
            {
                animatorEnemigo.SetBool("Atacando", true);
            }
            collision.gameObject.GetComponent<Personaje>().RecibirDano(danoDeAtaque, transform.position);
            atacando = true;
            Invoke("FinalizarAtaque", cooldownAtaque);
        }
    }

    internal virtual void FinalizarAtaque()
    {
        if (animatorEnemigo != null)
        {
            animatorEnemigo.SetBool("Atacando", false);
        }
        atacando = false;
    }

    internal void Flip()
    {
        transform.Rotate(0f, 180f, 0f);
    }
}
