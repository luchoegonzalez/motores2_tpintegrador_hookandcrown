using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{
    internal int vida = 3;
    internal float movimientoHorizontal = 0f;
    internal float movimientoVertical = 0f;
    internal Rigidbody2D rb;
    Vector3 velocidadActual = Vector3.zero;
    internal bool mirandoDerecha = true;
    GameManager gameManager;

    public bool controlandoPersonaje;
    public float rapidezMovimiento = 40f;
    [SerializeField] internal float suavidadVelocidad = 0.05f;

    [SerializeField] GameObject[] vidasUI;
    [SerializeField] float impulsoRecibirDano;
    internal bool recibiendoDano = false;
    internal float timerDano = 0f;

    Animator animatorPj;

    internal virtual void Awake()
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        rb = GetComponent<Rigidbody2D>();
        animatorPj = GetComponent<Animator>();
    }

    internal virtual void Update()
    {
        movimientoHorizontal = Input.GetAxis("Horizontal") * rapidezMovimiento;
    }

    internal virtual void FixedUpdate()
    {
        Moverse();
    }

    internal virtual void Moverse()
    {

        movimientoHorizontal *= Time.fixedDeltaTime;
        Vector3 velocidad = new Vector2(movimientoHorizontal * 10f, movimientoVertical);
        rb.velocity = Vector3.SmoothDamp(rb.velocity, velocidad, ref velocidadActual, suavidadVelocidad);

        if (movimientoHorizontal > 0 && !mirandoDerecha)
        {
            Flip();
        } else if (movimientoHorizontal < 0 && mirandoDerecha)
        {
            Flip();
        }
    }

    internal void Flip()
    {
        mirandoDerecha = !mirandoDerecha;

        transform.Rotate(0f, 180f, 0f);
    }

    internal virtual void RecibirDano(int dano, Vector3 posicionEnemigo)
    {
        animatorPj.SetBool("RecibiendoDano", true);

        recibiendoDano = true;
        timerDano = 0f;
        vida -= dano;
        GraficarVida();

        //rb.velocity = new Vector2(0, rb.velocity.y);
        Vector3 direccionEnemigo = transform.position - posicionEnemigo;
        direccionEnemigo.Normalize();
        rb.AddForce(new Vector2(direccionEnemigo.x, direccionEnemigo.y) * impulsoRecibirDano, ForceMode2D.Impulse);

        if (vida <= 0)
        {
            gameManager.Perdiste();
            gameObject.SetActive(false);
        }
    }

    internal virtual void PermitirMovimiento()
    {
        animatorPj.SetBool("RecibiendoDano", false);
        recibiendoDano = false;
        timerDano = 0f;
    }

    public void GraficarVida()
    {
        foreach(GameObject corazon in vidasUI)
        {
            corazon.SetActive(false);
        }
        for (int i = 0; i < vida; i++)
        {
            vidasUI[i].SetActive(true);
        }
    }

}
