using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalDeNivel : MonoBehaviour
{
    GameManager gameManager;
    public string nivel;

    private void Start()
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Rammel"))
        {
            gameManager.GameOver("Level " + nivel + " Completed");
            gameManager.nivelCompletado = true;

            GameData dataAlmacenada = SaveSystem.CargarNiveles();
            SaveSystem.GuardarNivel(gameManager, dataAlmacenada);
        }
    }
}
