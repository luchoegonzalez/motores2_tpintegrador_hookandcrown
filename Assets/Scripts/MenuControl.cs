using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MenuControl : MonoBehaviour
{
    public GameObject panelMenu;
    public GameObject panelOpciones;
    public GameObject panelNiveles;
    public Slider sliderMusica;
    public Slider sliderSonidos;
    public AudioMixer mixer;

    public AudioSource sonidoAplicado;

    public NivelSelector[] nivelSelectores;

    private void Start()
    {
        if (PlayerPrefs.HasKey("MusicVol"))
        {
            CargarVolumenes();
        } else
        {
            AplicarVolumenMusica();
            AplicarVolumenSonido();
        }

        CargarPartida();
    }

    public void VerNiveles()
    {
        panelNiveles.SetActive(true);
        panelMenu.SetActive(false);
        panelOpciones.SetActive(false);
    }

    public void Salir()
    {
        Application.Quit();
    }

    public void AbrirOpciones()
    {
        panelMenu.SetActive(false);
        panelOpciones.SetActive(true);
        panelNiveles.SetActive(false);
    }

    public void VolverAMenu()
    {
        panelMenu.SetActive(true);
        panelOpciones.SetActive(false);
        panelNiveles.SetActive(false);
    }

    public void AplicarVolumenMusica ()
    {
        float volumen = sliderMusica.value;
        mixer.SetFloat("Music", Mathf.Log10(volumen) * 20);
        PlayerPrefs.SetFloat("MusicVol", volumen);
    }

    public void AplicarVolumenSonido()
    {
        float volumen = sliderSonidos.value;
        mixer.SetFloat("Sounds", Mathf.Log10(volumen) * 20);
        PlayerPrefs.SetFloat("SoundsVol", volumen);
    }

    public void CargarVolumenes()
    {
        sliderMusica.value = PlayerPrefs.GetFloat("MusicVol");
        sliderSonidos.value = PlayerPrefs.GetFloat("SoundsVol");

        AplicarVolumenMusica();
        AplicarVolumenSonido();
    }

    public void JugarNivel(string nivel)
    {
        SceneManager.LoadScene(nivel);
    }

    public void CargarPartida()
    {
        GameData data = SaveSystem.CargarNiveles();

        if (data != null)
        {
            nivelSelectores[0].completado = data.nivel1.completado;
            nivelSelectores[0].cantidadLombrices = data.nivel1.cantidadLombrices;

            nivelSelectores[1].completado = data.nivel2.completado;
            nivelSelectores[1].cantidadLombrices = data.nivel2.cantidadLombrices;

            nivelSelectores[2].completado = data.nivel3.completado;
            nivelSelectores[2].cantidadLombrices = data.nivel3.cantidadLombrices;

            nivelSelectores[3].completado = data.nivel4.completado;
            nivelSelectores[3].cantidadLombrices = data.nivel4.cantidadLombrices;
        }
    }
}
