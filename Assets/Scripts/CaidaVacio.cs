using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaidaVacio : MonoBehaviour
{
    GameManager gameManager;
    Personaje aelarion;
    private void Awake()
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
    }
    private void Start()
    {
        aelarion = GameObject.Find("Aelarion").GetComponent<Personaje>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Rammel"))
        {
            collision.gameObject.GetComponent<Personaje>().RecibirDano(1, collision.transform.position);
            collision.gameObject.GetComponent<RammelControl>().Respawnear(gameManager.ultimoCheckpoint);
            aelarion.controlandoPersonaje = false;
            collision.gameObject.GetComponent<Personaje>().controlandoPersonaje = true;

            GetComponent<AudioSource>().pitch = Random.Range(0.9f, 1.1f);
            GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);
        }

        if (collision.gameObject.CompareTag("Caja"))
        {
            collision.gameObject.GetComponent<CajaControl>().RespawnearCaja();
            GetComponent<AudioSource>().pitch = Random.Range(0.9f, 1.1f);
            GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);
        }
    }
}
