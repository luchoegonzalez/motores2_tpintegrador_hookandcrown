using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecolectableControl : MonoBehaviour
{
    public float amplitud = 0.2f;
    public float amplitudSombra = 0.1f;
    public float frecuencia = 1f;

    private Vector3 posicionInicial;
    private Transform sombra;
    private Vector3 sombraPosicionInicial;
    private Vector3 sombraEscalaInicial;

    void Start()
    {
        posicionInicial = transform.position;
        sombra = transform.GetChild(0);
        sombraPosicionInicial = sombra.position;
        sombraEscalaInicial = sombra.localScale;
    }

    void Update()
    {
        float newY = posicionInicial.y + Mathf.Sin(Time.time * frecuencia) * amplitud;

        transform.position = new Vector3(posicionInicial.x, newY, posicionInicial.z);

        sombra.position = sombraPosicionInicial;

        float scaleAdjustment = 1 - ((newY - posicionInicial.y) * 0.5f / amplitudSombra);
        sombra.localScale = new Vector3(sombraEscalaInicial.x * scaleAdjustment, sombraEscalaInicial.y * scaleAdjustment, sombraEscalaInicial.z);
    }
}
