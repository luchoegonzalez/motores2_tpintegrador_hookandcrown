using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RammelControl : Personaje
{
    //basicos y movimiento
    bool jump = false;
    bool tocandoElSuelo = false;
    bool atacando = false;
    public float radioSuelo = 0.2f;
    Animator animator;
    private CapsuleCollider2D colliderCapsula;

    //salto
    public float fuerzaSalto = 10f;
    [SerializeField] private Transform chequeadorSuelo;
    [SerializeField] private LayerMask capaSuelo;

    //ataque
    public float rangoDeAtaque;
    public float alturaColliderAtaque;
    public float distanciaColliderAtaque;
    [SerializeField] private Transform puntoDeAtaque;
    [SerializeField] private LayerMask capaEnemigos;
    public float tasaDeAtaque = 2f;
    float tiempoSiguienteAtaque = 0f;

    //engancharse
    [SerializeField] private float longitudGancho;
    private Vector3 puntoDeAgarre;
    private DistanceJoint2D joint;
    bool enganchado = false;
    [SerializeField] private LineRenderer cuerda;
    [SerializeField] private Transform anclaDeCuerda;
    [SerializeField] private GameObject brazo;
    [SerializeField] private GameObject gancho;

    //planeo
    public bool planeando = false;

    //caida
    bool permitirAterrizaje = false;

    //sonidos
    AudioSource sourceSonidoPasos;
    AudioSource audioSource;
    public AudioClip[] clipsDePasos, clipsDeSalto;
    public AudioClip clipAtaque, clipAterrizaje, clipEnemigoGolpeado, clipDamage;
    public float tiempoEntrePasos = 0.3f;
    private float temporizadorSonidoPasos;

    internal override void Awake()
    {
        animator = GetComponent<Animator>();
        joint = GetComponent<DistanceJoint2D>();
        colliderCapsula = GetComponent<CapsuleCollider2D>();
        sourceSonidoPasos = GetComponent<AudioSource>();
        audioSource = transform.GetChild(4).GetComponent<AudioSource>();
        Desengancharse();
        base.Awake();
    }

    internal override void Update()
    {
        if (controlandoPersonaje && !recibiendoDano)
        {
            base.Update();
            animator.SetFloat("Rapidez", Mathf.Abs(movimientoHorizontal));
            if (Mathf.Abs(movimientoHorizontal) >= 0.001f && tocandoElSuelo && !recibiendoDano)
            {
                temporizadorSonidoPasos -= Time.deltaTime;

                if (temporizadorSonidoPasos <= 0)
                {
                    int indiceAleatorio = Random.Range(0, clipsDePasos.Length);
                    sourceSonidoPasos.pitch = Random.Range(0.8f, 1.2f);
                    sourceSonidoPasos.PlayOneShot(clipsDePasos[indiceAleatorio]);

                    temporizadorSonidoPasos = tiempoEntrePasos;
                }
            }
            else
            {
                temporizadorSonidoPasos = 0;
            }

            if (Input.GetButtonDown("Jump"))
            {
                jump = true;
            }

            if (Time.time >= tiempoSiguienteAtaque)
            {
                atacando = false;
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    if (enganchado)
                    {
                        Desengancharse();
                    } else
                    {
                        animator.SetTrigger("Atacar");
                        tiempoSiguienteAtaque = Time.time + 1f / tasaDeAtaque;
                    }
                }
            }
        } else
        {
            animator.SetFloat("Rapidez", 0);
            //sourceSonido.Stop();
            temporizadorSonidoPasos = 0;
        }

        if (atacando)
        {
            RaycastHit2D hit = Physics2D.BoxCast(puntoDeAtaque.position + transform.right * rangoDeAtaque * transform.localScale.x * distanciaColliderAtaque,
                new Vector3(colliderCapsula.bounds.size.x * rangoDeAtaque, colliderCapsula.bounds.size.y * alturaColliderAtaque, colliderCapsula.bounds.size.z),
                0, Vector2.left, 0, capaEnemigos);
            Engancharse(hit);
            GolpearEnemigos(hit);
        }
        if (enganchado)
        {
            cuerda.SetPosition(1, anclaDeCuerda.position);
            Vector3 direccionDeBrazo = cuerda.GetPosition(0) - brazo.transform.position;
            float angulo = Mathf.Atan2(direccionDeBrazo.y, direccionDeBrazo.x) * Mathf.Rad2Deg;
            brazo.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angulo - 70));

            if (movimientoHorizontal > 0 && !mirandoDerecha)
            {
                brazo.transform.localScale = new Vector3(0.33f,0.33f,1);
            }
            else if (movimientoHorizontal < 0 && mirandoDerecha)
            {
                brazo.transform.localScale = new Vector3(-0.33f, 0.33f, 1);
            }

            gancho.transform.position = cuerda.GetPosition(0);
            gancho.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angulo - 90));
        }

        if (recibiendoDano)
        {
            timerDano += Time.deltaTime;
            if (timerDano >= 0.25f && tocandoElSuelo)
            {
                PermitirMovimiento();
            }
        }

        ControlarPlaneo();
    }

    internal override void FixedUpdate()
    {
        ChequearSuelo();

        if (controlandoPersonaje && !recibiendoDano)
        {
            movimientoVertical = rb.velocity.y;

            if (!atacando || (atacando && !tocandoElSuelo))
            {
                base.FixedUpdate();
                ControlarSalto();
            } else
            {
                rb.velocity = new Vector2(0, rb.velocity.y);
            }

        } else
        {
            if (!recibiendoDano)
            {
                rb.velocity = new Vector2(0, rb.velocity.y);
            }
        }
    }

    void ControlarSalto()
    {
        if((tocandoElSuelo && jump) || (enganchado && jump))
        {
            tocandoElSuelo = false;
            animator.SetBool("Saltando", true);
            rb.AddForce(new Vector2(0f, fuerzaSalto));

            CancelInvoke("PermitirAterrizaje");
            Invoke("PermitirAterrizaje", 0.03f);
            audioSource.PlayOneShot(clipsDeSalto[Random.Range(0,clipsDeSalto.Length)]);;
        }

        if (jump)
        {
            Desengancharse();
        }
        jump = false;
    }

    void PermitirAterrizaje()
    {
        permitirAterrizaje = true;
    }

    void ChequearSuelo()
    {
        tocandoElSuelo = Physics2D.OverlapCircle(chequeadorSuelo.position, radioSuelo, capaSuelo);
        if (animator.GetBool("Cayendo") && tocandoElSuelo)
        {
            sourceSonidoPasos.PlayOneShot(clipAterrizaje);
            temporizadorSonidoPasos = tiempoEntrePasos;

            animator.SetBool("Cayendo", false);
            animator.SetTrigger("TocandoSuelo");
        }
        if (animator.GetBool("Saltando") && permitirAterrizaje && tocandoElSuelo)
        {
            sourceSonidoPasos.PlayOneShot(clipAterrizaje);
            temporizadorSonidoPasos = tiempoEntrePasos;

            animator.SetBool("Saltando", false);
            animator.SetTrigger("TocandoSuelo");
        }
        if (tocandoElSuelo)
        {
            CancelInvoke("PermitirAterrizaje");
            permitirAterrizaje = false;
        }
    }

    void ControlarPlaneo()
    {
        if(!tocandoElSuelo && !enganchado && Input.GetButton("Jump") && rb.velocity.y < 0 && !recibiendoDano)
        {
            planeando = true;
            rb.gravityScale = 0.5f;
        } else
        {
            planeando = false;
            rb.gravityScale = 3;
        }

        if (!tocandoElSuelo && !enganchado && rb.velocity.y < 0)
        {
            animator.SetBool("Cayendo", true);
            animator.SetBool("Saltando", false);
        }
    }

    void Atacar()
    {
        if (!enganchado)
        {
            atacando = true;
            audioSource.pitch = Random.Range(0.95f, 1.05f);
            audioSource.PlayOneShot(clipAtaque);
        }
    }

    void GolpearEnemigos(RaycastHit2D hit)
    {
        if (hit.collider != null)
        {
            if (hit.collider.CompareTag("Enemigo"))
            {
                hit.collider.GetComponent<Enemigo>().RecibirDano(1);

                audioSource.pitch = Random.Range(0.95f, 1.05f);
                audioSource.PlayOneShot(clipEnemigoGolpeado);
            }
        }
    }

    void Engancharse(RaycastHit2D hit)
    {
        if (hit.collider != null)
        {
            if (hit.collider.CompareTag("Gancho"))
            {
                atacando = false;
                puntoDeAgarre = hit.transform.position;
                puntoDeAgarre.z = 0;
                joint.connectedAnchor = puntoDeAgarre;
                joint.enabled = true;
                joint.distance = longitudGancho;
                enganchado = true;
                cuerda.enabled = true;
                cuerda.SetPosition(0, puntoDeAgarre);
                cuerda.SetPosition(1, anclaDeCuerda.position);
                animator.SetBool("Enganchado", true);
                brazo.SetActive(true);
                gancho.SetActive(true);
            }
        }
    }

    void Desengancharse()
    {
        joint.enabled = false;
        enganchado = false;
        cuerda.enabled = false;
        animator.SetBool("Enganchado", false);
        brazo.SetActive(false);
        gancho.SetActive(false);
    }

    public void Respawnear(Vector3 checkpoint)
    {
        rb.velocity = new Vector2(0, rb.velocity.y);
        transform.position = checkpoint;
    }

    internal override void RecibirDano(int dano, Vector3 posicionEnemigo)
    {
        Desengancharse();
        audioSource.pitch = Random.Range(0.95f, 1.05f);
        audioSource.PlayOneShot(clipDamage);
        base.RecibirDano(dano, posicionEnemigo);
    }

    internal override void PermitirMovimiento()
    {
        tiempoSiguienteAtaque = 0f;
        base.PermitirMovimiento();
    }

    private void OnDrawGizmos()
    {
        colliderCapsula = GetComponent<CapsuleCollider2D>();
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(chequeadorSuelo.position, radioSuelo);
        Gizmos.DrawWireCube(puntoDeAtaque.position + transform.right * rangoDeAtaque * transform.localScale.x * distanciaColliderAtaque,
            new Vector3(colliderCapsula.bounds.size.x * rangoDeAtaque, colliderCapsula.bounds.size.y * alturaColliderAtaque, colliderCapsula.bounds.size.z));
    }
}
