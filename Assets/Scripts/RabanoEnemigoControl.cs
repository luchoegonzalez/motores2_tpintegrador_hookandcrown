using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RabanoEnemigoControl : Enemigo
{
    [SerializeField] float rapidez = 2;
    public Vector3[] posiciones;
    private int posicionActual = 0;

    // Start is called before the first frame update
    internal override void Start()
    {
        base.Start();
        transform.position = posiciones[posicionActual];
        Flip();
    }

    // Update is called once per frame
    void Update()
    {
        Moverse();
    }

    void Moverse()
    {
        if (!atacando && !muerto)
        {
            transform.position = Vector3.MoveTowards(transform.position, posiciones[posicionActual], rapidez * Time.deltaTime);

            if (Vector3.Distance(transform.position, posiciones[posicionActual]) < 0.05f)
            {
                posicionActual = (posicionActual + 1) % posiciones.Length;
                Flip();
            }
        } 
    }
}
