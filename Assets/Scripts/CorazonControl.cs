using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorazonControl : RecolectableControl
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Rammel") || collision.CompareTag("Aelarion"))
        {
            if (collision.GetComponent<Personaje>().vida < 3)
            {
                collision.GetComponent<Personaje>().vida++;
                collision.GetComponent<Personaje>().GraficarVida();
                GetComponent<AudioSource>().Play();
                GetComponent<CapsuleCollider2D>().enabled = false;
                GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(0).gameObject.SetActive(false);
                Destroy(gameObject, 1f);
            }
        }
    }
}
