using System.Collections;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class GameData
{
    [System.Serializable]
    public struct Nivel
    {
        public bool completado;
        public int cantidadLombrices;
    }

    public Nivel nivel1, nivel2, nivel3, nivel4;

    // Constructor que recibe el GameManager y actualiza solo el nivel actual
    public GameData(GameManager gameManager, GameData previousData = null)
    {
        // Si existen datos previos, cargarlos
        if (previousData != null)
        {
            nivel1 = previousData.nivel1;
            nivel2 = previousData.nivel2;
            nivel3 = previousData.nivel3;
            nivel4 = previousData.nivel4;
        }

        // Actualizar solo el nivel actual con la información del GameManager
        switch (gameManager.nivelActual)
        {
            case 1:
                nivel1.completado = gameManager.nivelCompletado;
                nivel1.cantidadLombrices = gameManager.contadorLombrices;
                break;
            case 2:
                nivel2.completado = gameManager.nivelCompletado;
                nivel2.cantidadLombrices = gameManager.contadorLombrices;
                break;
            case 3:
                nivel3.completado = gameManager.nivelCompletado;
                nivel3.cantidadLombrices = gameManager.contadorLombrices;
                break;
            case 4:
                nivel4.completado = gameManager.nivelCompletado;
                nivel4.cantidadLombrices = gameManager.contadorLombrices;
                break;
        }
    }
}