using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CajaControl : MonoBehaviour
{
    Rigidbody2D rb;
    BoxCollider2D cajaCollider;
    public LayerMask capaCaja;
    public LayerMask capaEnemigos;
    public LayerMask capaSuelo;
    public PhysicsMaterial2D materialfisicas;
    Vector3 posicionInicial;

    AudioSource audioS;
    float timerSonido = 0;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        cajaCollider = GetComponent<BoxCollider2D>();
        audioS = GetComponent<AudioSource>();
        posicionInicial = transform.position;
    }

    
    void Update()
    {
        timerSonido -= Time.deltaTime;

        RaycastHit2D hitCaja = Physics2D.BoxCast(new Vector2(cajaCollider.bounds.center.x, cajaCollider.bounds.min.y - 0.1f), new Vector2(cajaCollider.size.x * 0.3f, 0.1f), 0, Vector3.down, 0.1f, capaCaja);
        if (hitCaja)
        {
            cajaCollider.sharedMaterial = null;
            rb.sharedMaterial = null;
        } else
        {
            cajaCollider.sharedMaterial = materialfisicas;
            rb.sharedMaterial = materialfisicas;
        }

        if (!cajaCollider.isTrigger)
        {
            RaycastHit2D hitEnemigo = Physics2D.BoxCast(new Vector2(cajaCollider.bounds.center.x, cajaCollider.bounds.min.y - 0.1f), new Vector2(cajaCollider.size.x * 0.3f, 0.1f), 0, Vector3.down, 0.1f, capaEnemigos);
            if (hitEnemigo.collider != null && hitEnemigo.collider.CompareTag("Enemigo") && rb.velocity.y < -0.01f)
            {
                hitEnemigo.collider.GetComponent<Enemigo>().RecibirDano(10);
            }

            //RaycastHit2D hitSuelo = Physics2D.BoxCast(new Vector2(cajaCollider.bounds.center.x, cajaCollider.bounds.min.y - 0.1f), new Vector2(cajaCollider.size.x * 0.3f, 0.1f), 0, Vector3.down, 0.1f, capaSuelo);
            //if ((hitSuelo || hitCaja) && rb.velocity.y < -0.01f)
            //{
            //    audioS.Play();
            //}
        }
    }

    public void RespawnearCaja()
    {
        transform.position = posicionInicial;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Suelo") || collision.gameObject.CompareTag("Caja"))
        {
            if(timerSonido <= 0)
            {
                audioS.Play();
                timerSonido = 1f;
            }
        }
    }

    //private void OnDrawGizmos()
    //{
    //    cajaCollider = GetComponent<BoxCollider2D>();
    //    Gizmos.DrawWireCube(new Vector3(cajaCollider.bounds.center.x + 0.15f, cajaCollider.bounds.min.y - 0.03f, cajaCollider.bounds.center.z), new Vector3(cajaCollider.size.x * 0.35f, 0.1f,1f));
    //}
}
