using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NubesContainerControl : MonoBehaviour
{
    float inicioY;
    public GameObject camara;
    public float valorParallaxY;


    void Start()
    {
        inicioY = transform.position.y;
    }

    void Update()
    {
        Vector3 posicionCamara = camara.transform.position;
        float distanciaY = posicionCamara.y * valorParallaxY;

        Vector3 nuevaPosicion = new Vector3(transform.position.x, inicioY + distanciaY, transform.position.z);

        transform.position = nuevaPosicion; 
    }
}
