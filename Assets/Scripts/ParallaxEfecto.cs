using System;
using UnityEngine;


public class ParallaxEfecto : ParallaxOlas
{
    internal override void Start()
    {
        base.Start();
        longitudSpriteX = GetComponent<SpriteRenderer>().bounds.size.x;
    }
    internal override void LateUpdate()
    {
        base.LateUpdate();

        if (posicionTemporalX > posicionInicialX + (longitudSpriteX / 2))
        {
            posicionInicialX += longitudSpriteX;
        }
        else if (posicionTemporalX < posicionInicialX - (longitudSpriteX / 2))
        {
            posicionInicialX -= longitudSpriteX;
        }
    }
}