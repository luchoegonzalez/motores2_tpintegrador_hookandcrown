using UnityEngine;

public class NubesControl : MonoBehaviour
{
    public float rapidez = 5f;
    public float inicioX = 10f;
    public float finX = -10f;



    void LateUpdate()
    {
        transform.Translate(Vector3.left * rapidez * Time.deltaTime, Space.Self);

        if (transform.localPosition.x <= finX)
        {
            transform.localPosition = new Vector3(inicioX, transform.localPosition.y, transform.localPosition.z);
        }
    }
}
