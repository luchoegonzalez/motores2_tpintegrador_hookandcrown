using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LombrizControl : RecolectableControl
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Rammel"))
        {
            GameObject.Find("Game Manager").GetComponent<GameManager>().contadorLombrices++;
            GetComponent<AudioSource>().Play();
            GetComponent<CircleCollider2D>().enabled = false;
            GetComponent<SpriteRenderer>().enabled = false;
            transform.GetChild(0).gameObject.SetActive(false);
            Destroy(gameObject, 1f);
        }
    }
}
