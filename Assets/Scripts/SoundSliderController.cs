using UnityEngine;
using UnityEngine.EventSystems;

public class SoundSliderController : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    public AudioSource source;
    public void OnPointerDown(PointerEventData eventData)
    {
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        source.Play();
    }
}
