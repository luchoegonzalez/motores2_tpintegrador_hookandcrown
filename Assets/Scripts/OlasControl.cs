using UnityEngine;

public class OlasControl : MonoBehaviour
{
    public GameObject olaPrefab;
    public SpriteRenderer spriteMar;
    public float intervaloMin = 0.5f;
    public float intervaloMax = 2f;
    public int cantidadOlas = 5;
    public GameObject olasPadre;

    private float timer;

    void Start()
    {
        timer = Random.Range(intervaloMin, intervaloMax);
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            for (int i = 0; i < cantidadOlas; i++)
            {
                GenerarOla();
            }
            timer = Random.Range(intervaloMin, intervaloMax);
        }
    }

    void GenerarOla()
    {
        Bounds bounds = spriteMar.bounds;
        Vector2 posicion = new Vector2(
            Random.Range(bounds.min.x, bounds.max.x),
            Random.Range(bounds.min.y, bounds.max.y - 4.5f)
        );

        GameObject ola = Instantiate(olaPrefab, posicion, Quaternion.identity, olasPadre.transform);

        // Obtener la duraci�n de la animaci�n y destruir la ola despu�s de ese tiempo
        Animator animator = ola.GetComponent<Animator>();
        float animDuration = animator.GetCurrentAnimatorStateInfo(0).length;
        Destroy(ola, animDuration);
    }

    void OnDrawGizmosSelected()
    {
        if (spriteMar != null)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireCube(spriteMar.bounds.center, spriteMar.bounds.size);
        }
    }
}